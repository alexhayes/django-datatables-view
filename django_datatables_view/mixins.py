from decimal import Decimal
import sys
import json

from django.conf import settings
from django.http import HttpResponse
from django.core.mail import mail_admins
from django.utils.encoding import force_unicode
from django.utils.functional import Promise
from django.utils.translation import ugettext as _
from django.utils.cache import add_never_cache_headers
from django.views.generic.base import TemplateView
from django.utils.importlib import import_module
from django.core.serializers.json import DjangoJSONEncoder

class DTEncoder(DjangoJSONEncoder):
    """Encodes django's lazy i18n strings and Decimals
    """
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_unicode(obj)
        elif isinstance(obj, Decimal):
            return force_unicode(obj)
        return DjangoJSONEncoder.default(self, obj)

JSON_ENCODER = getattr(settings, 'JSON_ENCODER', False)
if JSON_ENCODER:
    parts = JSON_ENCODER.split('.')
    module = import_module('.'.join(parts[:-1]))
    JSON_ENCODER = getattr(module, parts[-1:][0])
else:
    JSON_ENCODER = DTEncoder

class JSONResponseMixin(object):
    is_clean = False

    def render_to_response(self, context):
        """ Returns a JSON response containing 'context' as payload
        """
        return self.get_json_response(context)

    def get_json_response(self, content, **httpresponse_kwargs):
        """ Construct an `HttpResponse` object.
        """
        response = HttpResponse(content,
                            content_type='application/json',
                            **httpresponse_kwargs)
        add_never_cache_headers(response)
        return response

    def post(self, *args, **kwargs):
        return self.get(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.request = request
        response = None

        func_val = self.get_context_data(**kwargs)
        if not self.is_clean:
            assert isinstance(func_val, dict)
            response = dict(func_val)
            if 'result' not in response:
                response['result'] = 'ok'
        else:
            response = func_val


        return self.render_to_response(json.dumps(response, cls=JSON_ENCODER)) 


class JSONResponseView(JSONResponseMixin, TemplateView):
    pass
